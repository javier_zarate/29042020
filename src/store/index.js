import Vue from 'vue'
import Vuex from 'vuex'
import VuexI18n from 'vuex-i18n' // load vuex i18n module
import app from './modules/app'
import axios from 'axios'
import VueAxios from 'vue-axios'

/*var json_coin_p = await axios.get('/datocoin.json');
json_coin_p = this.json_coin_p.data;
*/
import * as getters from './getters'

Vue.use(Vuex)



const store = new Vuex.Store({
  strict: true, // process.env.NODE_ENV !== 'production',
  getters,
  modules: {
    app,
  },
  state: {
    user_stored: "novalue",
    role_stored: "norole",
    bank_stored: [
      {code_bank : "002", name_bank : "BCP", entidad_bank:"Bancaria", state_bank:"ACTIVO"},
      {code_bank : "003", name_bank : "Interbank", entidad_bank:"Bancaria", state_bank:"ACTIVO"},
      {code_bank : "007", name_bank : "Citibank", entidad_bank:"Bancaria", state_bank:"ACTIVO"},
      {code_bank : "009", name_bank : "Scotia", entidad_bank:"Bancaria", state_bank:"ACTIVO"},
      {code_bank : "011", name_bank : "Continental", entidad_bank:"Bancaria", state_bank:"ACTIVO"},
      {code_bank : "018", name_bank : "Banco de la Nacion", entidad_bank:"Bancaria", state_bank:"ACTIVO"},
      {code_bank : "023", name_bank : "Comercio", entidad_bank:"Bancaria", state_bank:"ACTIVO"},
      {code_bank : "035", name_bank : "Financiero", entidad_bank:"Bancaria", state_bank:"ACTIVO"},
      {code_bank : "038", name_bank : "Banbif", entidad_bank:"Bancaria", state_bank:"ACTIVO"},
      {code_bank : "043", name_bank : "Crediscotia", entidad_bank:"Bancaria", state_bank:"ACTIVO"},
      {code_bank : "049", name_bank : "Mi Banco", entidad_bank:"Bancaria", state_bank:"ACTIVO"},
      {code_bank : "053", name_bank : "Banco GNB", entidad_bank:"Bancaria", state_bank:"ACTIVO"},
      {code_bank : "054", name_bank : "Falabella", entidad_bank:"Bancaria", state_bank:"ACTIVO"},
      {code_bank : "056", name_bank : "Santander", entidad_bank:"Bancaria", state_bank:"ACTIVO"},
      {code_bank : "058", name_bank : "Azteca", entidad_bank:"Bancaria", state_bank:"ACTIVO"},
      {code_bank : "800", name_bank : "Caja metropolitana", entidad_bank:"Bancaria", state_bank:"ACTIVO"},
      {code_bank : "801", name_bank : "Caja Piura", entidad_bank:"Bancaria", state_bank:"ACTIVO"},
      {code_bank : "802", name_bank : "Caja Trujillo", entidad_bank:"Bancaria", state_bank:"ACTIVO"},
      {code_bank : "803", name_bank : "Caja Arequipa", entidad_bank:"Bancaria", state_bank:"ACTIVO"},
      {code_bank : "805", name_bank : "Caja Sullana", entidad_bank:"Bancaria", state_bank:"ACTIVO"},
      {code_bank : "808", name_bank : "Caja Huancayo", entidad_bank:"Bancaria",state_bank:"ACTIVO"}
    ],
    typetransfer_stored: [
      {code_typetransfer : "320", name_typetransfer : "Abono Cta", state_typetransfer:"ACTIVO"},
      {code_typetransfer : "325", name_typetransfer : "Pago Tarj. Cred", state_typetransfer:"ACTIVO"}
    ],
    process_stored: [
      {code_process : "800000", name_process : "Consulta"},
      {code_process : "490000", name_process : "Transferencia"}
    ],
    responsecodelong_stored: [
      {code_responsecodelong : "0000", name_responsecodelong : "Successful"},
      {code_responsecodelong : "0101", name_responsecodelong : "Error1"},
      {code_responsecodelong : "0137", name_responsecodelong : "Error2"},
      {code_responsecodelong : "9915", name_responsecodelong : "Error3"},
      {code_responsecodelong : "9916", name_responsecodelong : "Error4"},
    ],
    channel_stored: [
      {code_channel : "02", name_channel : "ATM", entidad_channel:"Bancaria",state_channel:"ACTIVO"},
      {code_channel : "04", name_channel : "BOX", entidad_channel:"Bancaria",state_channel:"ACTIVO"},
      {code_channel : "07", name_channel : "IVR", entidad_channel:"Bancaria",state_channel:"ACTIVO"},
      {code_channel : "14", name_channel : "POS", entidad_channel:"Bancaria",state_channel:"ACTIVO"},
      {code_channel : "15", name_channel : "WEB", entidad_channel:"Bancaria",state_channel:"ACTIVO"},
      {code_channel : "18", name_channel : "Term. Admin.", entidad_channel:"Bancaria",state_channel:"ACTIVO"},
      {code_channel : "51", name_channel : "MasterCard", entidad_channel:"Bancaria",state_channel:"ACTIVO"},
      {code_channel : "52", name_channel : "NET", entidad_channel:"Bancaria",state_channel:"ACTIVO"},
      {code_channel : "54", name_channel : "Kiosco", entidad_channel:"Bancaria",state_channel:"ACTIVO"},
      {code_channel : "55", name_channel : "HEPS", entidad_channel:"Bancaria",state_channel:"ACTIVO"},
      {code_channel : "56", name_channel : "Ent. Financ.", entidad_channel:"Bancaria",state_channel:"ACTIVO"},
      {code_channel : "90", name_channel : "Ventanilla", entidad_channel:"Bancaria",state_channel:"ACTIVO"}
    ],
    coin_stored: [
      {code_coin : "604", name_coin : "Soles", symbol_coin:"S./",state_coin:"ACTIVO"},
      {code_coin : "840", name_coin : "Dolares", symbol_coin:"$",state_coin:"ACTIVO"}
    ],
    originresponsecode_stored: [
      {code_originresponsecode : "001", name_originresponsecode : "RESPONSECODE01"},
      {code_originresponsecode : "002", name_originresponsecode : "RESPONSECODE02"},
      {code_originresponsecode : "100", name_originresponsecode : "RESPONSECODE03"}

    ]
  },
  getters: {
    get_banks_stored (state){
      return state.bank_stored;
    },
    get_typetransfer_stored (state){
      return state.typetransfer_stored;
    },
    get_process_stored (state){
      return state.process_stored;
    },
    get_responsecodelong_stored (state){
      return state.responsecodelong_stored;
    },
    get_channels_stored (state){
      return state.channel_stored;
    },
    get_coins_stored (state){
      return state.coin_stored;
    },
    get_originresponsecode_stored (state){
      return state.originresponsecode_stored;
    },
    get_role_stored (state){
      return state.role_stored;
    },   
  },
  mutations: {
    save_user(state,user_logged){
      state.user_stored = user_logged;
    },
    save_role(state,role_asigned){
      state.role_stored = role_asigned;
    },
    save_coin(state,coin_data){
      //after read json data
      state.coin_stored = coin_data;
    },
    save_bank(state,bank_data){
      //after read json data
      state.bank_stored =[];
    },
    save_channel(state,channel_data){
      //after read json data
      state.channel_stored = channel_data;
    },
    add_single_channel(state,single_channel){
      state.channel_stored.push({code_channel : single_channel[0], name_channel : single_channel[1], entidad_channel: single_channel[2], state_channel: single_channel[3]});
    }
  },
})

Vue.use(VuexI18n.plugin, store)

export default store
