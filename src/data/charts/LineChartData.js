import { hex2rgb } from '../../services/vuestic-ui'

const generateValue = () => {
  return Math.floor(Math.random() * 100)
}

const generateYLabels = () => {
  const flip = !!Math.floor(Math.random() * 2)
  return flip ? ['Sucess', 'Failed'] : ['Sucess', 'Failed']
}

const generateArray = (length) => {
  return Array.from(Array(length), generateValue)
}

const getSize = () => {
  const minSize = 4
  return Math.max(minSize, new Date().getMonth())
}

let generatedData
let firstMonthIndex = 0

export const getLineChartData = (themes, firstMonth) => {
  const size = 9
  const months = ['this hour', '3 hours ago', '6 hours ago', '9 hours ago', '12 hours ago', '15 hours ago', '18 hours ago', '21 hours ago', '24 hours ago' ]
  const yLabels = generateYLabels()

  if (generatedData) {
    generatedData.datasets[0].backgroundColor = hex2rgb(themes.primary, 0.6).css
    generatedData.datasets[1].backgroundColor = hex2rgb(themes.info, 0.6).css
    if (firstMonth && firstMonthIndex !== firstMonth) {
      generatedData.labels.shift()
      generatedData.datasets.forEach((dataset) => {
        dataset.data.shift()
      })
      firstMonthIndex = firstMonth
    }
  } else {
    generatedData = {
      labels: months.splice(firstMonthIndex, size),
      datasets: [
        {
          label: yLabels[0],
          backgroundColor: hex2rgb(themes.primary, 0.6).css,
          borderColor: 'transparent',
          data: generateArray(size - firstMonthIndex),
        },
        {
          label: yLabels[1],
          backgroundColor: hex2rgb(themes.info, 0.6).css,
          borderColor: 'transparent',
          data: generateArray(size),
        },
      ],
    }
  }

  return generatedData
}
