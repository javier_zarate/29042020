import Vue from 'vue'
import users from '../../../public/datojson.json'
import axios from 'axios'
import VueAxios from 'vue-axios'
import dataTables from './../../components/data-tables/DataTables.vue'

//var data_imported = new dataTables();

export const getVerticalBarChartData = (themes) => ({
  labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
  datasets: [
    {
      label: 'USA',
      backgroundColor: themes.primary,
      borderColor: 'transparent',
      data: [50, 20, 12, 39, 10, 40, 39, 80, 40, 20, 12, 11],
    },
    {
      label: 'USSR',
      backgroundColor: themes.info,
      borderColor: 'transparent',
      data: [50, 10, 22, 39, 15, 20, 85, 32, 60, 50, 20, 30],
    },
  ],
})

/*var dtable1 = new dataTables();
var dtable_array = dtable1.send_data;
var day_to_mseg = 24*60*60*1000;
var response_code = ["0000","8001","4002"]

export const getVerticalBarChartData = (themes) => ({
  labels: ['Today', 'Yesterday', '2 days ago', '3 days ago', '4 days ago', '5 days ago', '6 days ago'],
  datasets: [
    {
      label: '0000',
      backgroundColor: themes.primary,
      borderColor: 'transparent',
      data: [this.er0000_0_ago.length, this.er0000_1_ago.length, this.er0000_2_ago.length, this.er0000_3_ago.length, this.er0000_4_ago.length, this.er0000_5_ago.length, this.er0000_6_ago.length],
    },
    {
      label: '8001',
      backgroundColor: themes.info,
      borderColor: 'transparent',
      data: [this.er8001_0_ago.length, this.er8001_1_ago.length, this.er8001_2_ago.length, this.er8001_3_ago.length, this.er8001_4_ago.length, this.er8001_5_ago.length, this.er8001_6_ago.length],
    },
    {
      label: '4002',
      backgroundColor: themes.info,
      borderColor: 'transparent',
      data: [this.er4002_0_ago.length, this.er4002_1_ago.length, this.er4002_2_ago.length, this.er4002_3_ago.length, this.er4002_4_ago.length, this.er4002_5_ago.length, this.er4002_6_ago.length],
    }
  ],
  name: 'Report',
  mode: 'history',
  data (){
    return{
      data_proc: dtable_array,
      campo04_data:[],
      reg_today:[],
      reg_1_ago:[],
      reg_2_ago:[],
      reg_3_ago:[],
      reg_4_ago:[],
      reg_5_ago:[],
      reg_6_ago:[],
    }
  },
  methods: {
    get_statistic(){
      var dates_statistics =[];
      var array_today = [];
      var current_date = new Date();
      var date_bef_1 = new Date(current_date.getTime()-day_to_mseg);
      var date_bef_2 = new Date(current_date.getTime()-2*day_to_mseg);
      var date_bef_3 = new Date(current_date.getTime()-3*day_to_mseg);
      var date_bef_4 = new Date(current_date.getTime()-4*day_to_mseg);
      var date_bef_5 = new Date(current_date.getTime()-5*day_to_mseg);
      var date_bef_6 = new Date(current_date.getTime()-6*day_to_mseg);
        
      var bef_0_string = current_date.getFullYear().toString() + ("0" + (current_date.getMonth()+1)).slice(-2)+("0" + current_date.getDate()).slice(-2);
      var bef_1_string = date_bef_1.getFullYear().toString() + ("0" + (date_bef_1.getMonth()+1)).slice(-2)+("0" + date_bef_1.getDate()).slice(-2);
      var bef_2_string = date_bef_2.getFullYear().toString() + ("0" + (date_bef_2.getMonth()+1)).slice(-2)+("0" + date_bef_2.getDate()).slice(-2);
      var bef_3_string = date_bef_3.getFullYear().toString() + ("0" + (date_bef_3.getMonth()+1)).slice(-2)+("0" + date_bef_3.getDate()).slice(-2);
      var bef_4_string = date_bef_4.getFullYear().toString() + ("0" + (date_bef_4.getMonth()+1)).slice(-2)+("0" + date_bef_4.getDate()).slice(-2);
      var bef_5_string = date_bef_5.getFullYear().toString() + ("0" + (date_bef_5.getMonth()+1)).slice(-2)+("0" + date_bef_5.getDate()).slice(-2);
      var bef_6_string = date_bef_6.getFullYear().toString() + ("0" + (date_bef_6.getMonth()+1)).slice(-2)+("0" + date_bef_6.getDate()).slice(-2);

      this.er0000 = this.data_proc.filter(data =>data.campo11  === response_code[0]);

      this.er0000_0_ago = this.er0001.filter(data =>data.campo04  === bef_0_string);
      this.er0000_1_ago = this.er0001.filter(data =>data.campo04  === bef_1_string);
      this.er0000_2_ago = this.er0001.filter(data =>data.campo04  === bef_2_string);
      this.er0000_3_ago = this.er0001.filter(data =>data.campo04  === bef_3_string);
      this.er0000_4_ago = this.er0001.filter(data =>data.campo04  === bef_4_string);
      this.er0000_5_ago = this.er0001.filter(data =>data.campo04  === bef_5_string);
      this.er0000_6_ago = this.er0001.filter(data =>data.campo04  === bef_6_string);

      this.er8001 = this.data_proc.filter(data =>data.campo11  === response_code[1]);

      this.er8001_0_ago = this.er8001.filter(data =>data.campo04  === bef_0_string);
      this.er8001_1_ago = this.er8001.filter(data =>data.campo04  === bef_1_string);
      this.er8001_2_ago = this.er8001.filter(data =>data.campo04  === bef_2_string);
      this.er8001_3_ago = this.er8001.filter(data =>data.campo04  === bef_3_string);
      this.er8001_4_ago = this.er8001.filter(data =>data.campo04  === bef_4_string);
      this.er8001_5_ago = this.er8001.filter(data =>data.campo04  === bef_5_string);
      this.er8001_6_ago = this.er8001.filter(data =>data.campo04  === bef_6_string);
      
      this.er4002 = this.data_proc.filter(data =>data.campo11  === response_code[2]);

      this.er4002_0_ago = this.er4002.filter(data =>data.campo04  === bef_0_string);
      this.er4002_1_ago = this.er4002.filter(data =>data.campo04  === bef_1_string);
      this.er4002_2_ago = this.er4002.filter(data =>data.campo04  === bef_2_string);
      this.er4002_3_ago = this.er4002.filter(data =>data.campo04  === bef_3_string);
      this.er4002_4_ago = this.er4002.filter(data =>data.campo04  === bef_4_string);
      this.er4002_5_ago = this.er4002.filter(data =>data.campo04  === bef_5_string);
      this.er4002_6_ago = this.er4002.filter(data =>data.campo04  === bef_6_string);

      


        
    }
  },
  watch: {
    dtable_array() {
      this.get_statistic();
    }
  },
  
})
*/