import Vue from 'vue'
import Vuex from 'vuex'
import {mapGetters} from 'vuex'
import Router from 'vue-router'
import AuthLayout from '../components/auth/AuthLayout'
import AppLayoutAdmin from '../components/admin/AppLayout'
import AppLayoutOperator from '../components/operator/AppLayout'

import AuthService from "./../msal_auth/authad"
import store from "./../store/index"


Vue.use(Router)

const EmptyParentComponent = {
  template: '<router-view></router-view>',
}
var null_value = "novalue";
const demoRoutes = []

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      redirect: { name: 'auth' },
    },
    {
      path: '/auth',
      name: 'auth',
      //component: AuthLayout,
      component: () => import('../components/auth/AuthLayout.vue'),
      meta: {
        forgetlogin: true
      }
    },
    {
      name: 'Admin',
      path: '/admin',
      component: AppLayoutAdmin,
      children: [
        {
          name: 'admin_dashboard',
          path: 'dashboard',
          component: () => import('../components/dashboard/Dashboard.vue'),
          default: true,
          meta: {
            beforeAdminDashboard: true
          }
        },
        {
          name: 'admin_parameters',
          path: 'parameters',
          component: EmptyParentComponent,
          children: [
            {
              name: 'admin_editarparametros',
              path: 'editparameters',
              component: () => import('../components/parameters/editparameters/EditParameters.vue'),
              //wikiLink: 'https://github.com/epicmaxco/vuestic-admin/wiki/Charts',
              meta: {
                beforeAdminEditParameters: true
              }
            },
          ],
          meta: {
            beforeAdminParameters: true
          }
        },
        {
          name: 'admin_tables',
          path: 'tables',
          component: EmptyParentComponent,
          children: [
            {
              name: 'admin_HistoricData',
              path: 'historicdata',
              component: () => import('../components/historic_data/HistoricData.vue'),
              //wikiLink: 'https://github.com/epicmaxco/vuestic-admin/wiki/Tables', // TODO Update docs
              meta: {
                beforeAdminHistoricData: true
              }
            },
            {
              name: 'admin_DailyData',
              path: 'dailydata',
              component: () => import('../components/daily_data/DailyData.vue'),
              //wikiLink: 'https://github.com/epicmaxco/vuestic-admin/wiki/Tables', // TODO Add docs
              meta: {
                beforeAdminDailyData: true
              }
            },
          ],
          meta: {
            beforeAdminTable: true
          }
        },
      ],
      meta: {
        beforeAdmin: true
      }
    },
    {
      name: 'Operator',
      path: '/operator',
      component: AppLayoutOperator,
      children: [
        {
          name: 'operator_dashboard',
          path: 'dashboard',
          component: () => import('../components/dashboard/Dashboard.vue'),
          default: true,
          meta: {
            beforeOperatorDailyData: true
          }
        },
        {
          name: 'operator_tables',
          path: 'tables',
          component: EmptyParentComponent,
          children: [
            {
              name: 'operator_HistoricData',
              path: 'historicdata',
              component: () => import('../components/historic_data/HistoricData.vue'),
              //wikiLink: 'https://github.com/epicmaxco/vuestic-admin/wiki/Tables', // TODO Update docs
              meta: {
                beforeOperatorHistoricData: true
              }
            },
            {
              name: 'operator_DailyData',
              path: 'dailydata',
              component: () => import('../components/daily_data/DailyData.vue'),
              //wikiLink: 'https://github.com/epicmaxco/vuestic-admin/wiki/Tables', // TODO Add docs
              meta: {
                beforeOperatorDailyData: true
              }
            },
          ],
          meta: {
            beforeOperatorTable: true
          }
        },
      ],
      meta: {
        beforeOperator: true
      }
    },
  ]
})

router.beforeEach((to, from, next) =>{
  var stored_user = new AuthService();
  var user_saved = stored_user.getlogged();
  var some_user = store.state.user_stored;
  var read_role = store.state.role_stored;

  if(to.matched.some(record =>record.meta.beforeAdmin) || to.matched.some(record =>record.meta.beforeAdminDashboard) || to.matched.some(record =>record.meta.beforeAdminParameters) || to.matched.some(record =>record.meta.beforeAdminEditParameters) || to.matched.some(record =>record.meta.beforeAdminTable) || to.matched.some(record =>record.meta.beforeAdminDailyData) || to.matched.some(record =>record.meta.beforeAdminHistoricData)){
    console.log(user_saved);
    console.log(read_role);    
    if(!user_saved || read_role!="admin"){
      next({
        name: 'auth'
      })
    }
    else{
      next();
    }
  }
  else{
    next();
  };
  if(to.matched.some(record =>record.meta.beforeOperator)){
    console.log(user_saved);
    console.log(read_role);    
    if(!user_saved || read_role!="operator"){
      next({
        name: 'auth'
      })
    }
    else{
      next();
    }
  }
  else{
    next();
  };
  if(to.matched.some(record =>record.meta.forgetlogin)){
    console.log(user_saved);  
    if(user_saved){
      store.commit('save_user',null_value);
      stored_user.logout();
      //store.state.user_stored
      next();
    }
    else{
      next();
    }
  }
  else{
    next();
  };
})

export default router
