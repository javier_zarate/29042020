import * as Msal from 'msal';

export default class AuthService {
  //default constructor
  constructor(){
    var redirectURI= "http://localhost:8081/"
    this.applicationConfig= {
      clientID: "ff57713a-d8e0-49a2-9edf-e76da7bb8d95",
      graphScopes: ['user.read']
    };
    this.app = new Msal.UserAgentApplication(
      this.applicationConfig.clientID,
      '',
      () => {
        // here the callback for login redirect
      },
      {
      redirectURI
      }
    );
  }
  login() {
    return this.app.loginPopup(this.applicationConfig.graphScopes).then(
      idToken => {
        const user = this.app.getUser();
        if (user) {
          return user;
        } 
        else {
          return null;
        }
      },
      () => {
        return null;
      }
    );
  };
  getlogged() {
    const userlogged = this.app.getUser();
    if (userlogged) {
      return userlogged;
    } 
    else {
      return null;
    }
  };
  logout(){
    this.app.logout();
  };
  getToken() {
    return this.app.acquireTokenSilent(this.applicationConfig.graphScopes).then(
      accessToken => {
        return accessToken;
      },
      error => {
        return this.app
          .acquireTokenPopup(this.applicationConfig.graphScopes)
          .then(
            accessToken => {
              return accessToken;
            },
            err => {
              console.error(err);
            }
          );
      }
    );
  };
}  





